# Tornado Examples

This are basic examples of how to utilize [Tornado] to create dynamic websites
for the [CSE 10001 Principles of Computing (Fall 2019)] course.

[Tornado]: https://www.tornadoweb.org/en/stable/
[CSE 10001 Principles of Computing (Fall 2019)]: https://www3.nd.edu/~pbui/teaching/cse.10001.sp19/
