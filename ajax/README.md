# ajax

This is a simple AJAX example in [Tornado].

To run in normal mode, do the following:

    $ ./ajax.py

To run in debugging mode, do the following:

    $ ./ajax.py --logging=debug
    
By default this application listens on port `9999`.
    
[Tornado]: https://www.tornadoweb.org
