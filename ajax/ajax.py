#!/usr/bin/env python3

import json
import os

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9999

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('ajax.html')

class UsersHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(json.dumps({
            'users': sorted(set(line.split()[0] for line in os.popen('who')))
        }))

# Application

Application = tornado.web.Application([
    (r'/users', UsersHandler),
    (r'/', IndexHandler),
])
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
