#!/usr/bin/env python3

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9999

# Handlers

class FormHandler(tornado.web.RequestHandler):
    def get(self):
        # Get value for name argument from form submission
        name = self.get_argument('name', '')

        # Display greeting if Name is specified
        if name:
            self.write('<b>Hello, {}</b>'.format(name))

        # To get input from the user, we must create a form where each input
        # element has a name we can use to retrieve data.
        self.write('''
<h1>Enter Your Name:</h1>
<form>
    <input type="text" name="name" value="{}">
    <input type="submit" value="Echo!">
</form>
'''.format(name))

# Application

Application = tornado.web.Application([
    (r'/', FormHandler),
])
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
