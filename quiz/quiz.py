#!/usr/bin/env python3

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9998

# Handlers

class QuizHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('quiz.html')

class ResultsHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('results.html',
            color    = self.get_argument('color', ''),
            leggings = self.get_argument('leggings', ''),
            eat      = self.get_arguments('eat'),
            pokemon  = self.get_argument('pokemon', ''),
        )

# Application

Application = tornado.web.Application([
    (r'/results', ResultsHandler),
    (r'/'       , QuizHandler),
], debug=True)
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
