# routing

This is a simple routing example in [Tornado].

To run in normal mode, do the following:

    $ ./routing.py

To run in debugging mode, do the following:

    $ ./routing.py --logging=debug
    
By default this application listens on port `9999`.

## Resources

- [Structure of a Tornado web application](https://www.tornadoweb.org/en/stable/guide/structure.html)
    
[Tornado]: https://www.tornadoweb.org
