#!/usr/bin/env python3

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9999

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Index!')

class PageHandler(tornado.web.RequestHandler):
    def get(self, page):        # Specify argument from URL
        self.write('Page {}!'.format(page))

# Application

Application = tornado.web.Application([
    (r'/page/(.*)', PageHandler),    # Use regular expression to express URL to match
    (r'/'         , IndexHandler),
])
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
