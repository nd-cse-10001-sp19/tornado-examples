# static

This is a simple static files example in [Tornado].

To run in normal mode, do the following:

    $ ./static.py

To run in debugging mode, do the following:

    $ ./static.py --logging=debug
    
By default this application listens on port `9999`.

## Resources

- [tornado.web.StaticFileHandler](https://www.tornadoweb.org/en/stable/web.html?highlight=static#tornado.web.StaticFileHandler)
    
[Tornado]: https://www.tornadoweb.org
