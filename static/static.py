#!/usr/bin/env python3

import os

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9999

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Index!')

# Application

Application = tornado.web.Application([
    # Utilize the built-in static file handler
    (r'/files/(.*)', tornado.web.StaticFileHandler, {'path': os.curdir}),
    (r'/'          , IndexHandler),
])
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
