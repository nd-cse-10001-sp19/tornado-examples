#!/usr/bin/env python3

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT = 9999

# Handlers

class TemplateHandler(tornado.web.RequestHandler):
    def get(self):
        name = self.get_argument('name', '')
        self.render('template.html', name=name)     # Render template with arguments

# Application

Application = tornado.web.Application([
    (r'/', TemplateHandler),
])
Application.listen(PORT)

# Main Execution

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
